﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace taskSolution.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string UserIdAddress { get; set; }
    }
}