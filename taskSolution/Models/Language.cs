﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace taskSolution.Models
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Language
    {
        [Key]
        public int GeoDataId { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("native")]
        public string Native { get; set; }
    }
}