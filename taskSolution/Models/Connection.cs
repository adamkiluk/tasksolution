﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace taskSolution.Models
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Connection
    {
        [Key]
        public int Id { get; set; }

        [JsonProperty("asn")]
        public long Asn { get; set; }

        [JsonProperty("isp")]
        public string Isp { get; set; }
    }
}