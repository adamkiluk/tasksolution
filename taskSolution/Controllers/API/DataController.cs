﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Results;
using taskSolution.Application;
using taskSolution.Models;

namespace taskSolution.Controllers.API
{
    [RoutePrefix("api/Data")]
    public class DataController : ApiController
    {
        private TaskDbContext _dbContext;
        private Validation validator;

        public DataController()
        {
            _dbContext = new TaskDbContext();
            validator = new Validation();
        }

        //POST: api/Data/Post
        [HttpPost]
        [Route("Post")]
        public JsonResult<GeoData> Post(string userIP)
        {
            bool ifExists = _dbContext.Datas.Any(x => x.Ip == userIP);

            if (validator.IpValidation(userIP) && !ifExists)
            {
                string ipAddress = String.Concat("http://api.ipstack.com/", userIP ,"?access_key=686f068fe5371e9e78b8b0034918d1a0&format=1");

                WebRequest webRequest = WebRequest.Create(ipAddress);
                WebResponse response = webRequest.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);

                string responseFromServer = reader.ReadToEnd();

                JObject parsedString = JObject.Parse(responseFromServer);

                var jsonData = parsedString.ToObject<GeoData>();

                _dbContext.Datas.Add(jsonData);
                _dbContext.SaveChanges();

                jsonData.ErrorMessage = "Status OK";
                return Json(jsonData);

            }

            //here we can return json with error msg if validation fails
            var entityError = new GeoData();
            entityError.ErrorMessage = "Status ERROR";
            return Json(entityError);

        }

        //POST: api/Data/DeleteSpecifiedAddress
        [HttpPost]
        [Route("DeleteSpecifiedAddress")]
        public JsonResult<GeoData> DeleteSpecifiedAddress(string userIP)
        {
            bool ifExists = _dbContext.Datas.Any(x => x.Ip == userIP);

            if (validator.IpValidation(userIP)&& ifExists)
            {
                var entityToDelete = _dbContext.Datas.Where(n => n.Ip == userIP)
                        .First();

                _dbContext.Datas.Remove(entityToDelete);
                _dbContext.SaveChanges();

                entityToDelete.ErrorMessage = "Status OK";
                return Json(entityToDelete);
            }

            //here we can return json with error msg if validation fails
            var entityError = new GeoData();
            entityError.ErrorMessage = "Status ERROR";
            return Json(entityError);
        }

        // GET: api/Data/GetAll
        [Route("GetAll")]
        public List<string> GetAll()
        {
            var IpList = _dbContext.Datas.Select(n => n.Ip).ToList();

            if (IpList == null)
            {
                return new List<string>(new string[] { "Status ERROR"});
            }
            return IpList;
        }

        //GET: api/Data/GetBySpecifiedAddress
        [Route("GetBySpecifiedAddress")]
        public JsonResult<GeoData> GetSpecifiedAddress(string userIP)
        {
            if (validator.IpValidation(userIP))
            {
                var data = _dbContext.Datas.Where(n => n.Ip == userIP)
                        .FirstOrDefault<GeoData>();
                if (data == null)
                {
                    var newData = new GeoData();

                    newData.ErrorMessage = "Status ERROR";
                    return Json(newData);
                }

                data.ErrorMessage = "Status OK";
                return Json(data);
            }

            //here we can return json with error msg if validation fails
            var entityError = new GeoData();
            entityError.ErrorMessage = "Status ERROR";
            return Json(entityError);
        }
    }
}
