namespace taskSolution.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DbSeed : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GeoDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ip = c.String(),
                        Hostname = c.String(),
                        Type = c.String(),
                        ContinentCode = c.String(),
                        ContinentName = c.String(),
                        CountryCode = c.String(),
                        CountryName = c.String(),
                        RegionCode = c.String(),
                        RegionName = c.String(),
                        City = c.String(),
                        Zip = c.String(),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Location_GeoDataId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.Location_GeoDataId)
                .Index(t => t.Location_GeoDataId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        GeoDataId = c.Int(nullable: false, identity: true),
                        GeonameId = c.Long(nullable: false),
                        Capital = c.String(),
                        CountryFlag = c.String(),
                        CountryFlagEmoji = c.String(),
                        CountryFlagEmojiUnicode = c.String(),
                        CallingCode = c.Long(nullable: false),
                        IsEu = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.GeoDataId);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        GeoDataId = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Native = c.String(),
                        Location_GeoDataId = c.Int(),
                    })
                .PrimaryKey(t => t.GeoDataId)
                .ForeignKey("dbo.Locations", t => t.Location_GeoDataId)
                .Index(t => t.Location_GeoDataId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserIdAddress = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GeoDatas", "Location_GeoDataId", "dbo.Locations");
            DropForeignKey("dbo.Languages", "Location_GeoDataId", "dbo.Locations");
            DropIndex("dbo.GeoDatas", new[] { "Location_GeoDataId" });
            DropIndex("dbo.Languages", new[] { "Location_GeoDataId" });
            DropTable("dbo.Users");
            DropTable("dbo.Languages");
            DropTable("dbo.Locations");
            DropTable("dbo.GeoDatas");
        }
    }
}
