﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using taskSolution.Models;

namespace taskSolution
{
    public class TaskDbContext : DbContext
    {
        public DbSet<GeoData> Datas { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<User>Users { get; set; }
        public TaskDbContext() : base("name=TaskDbContext")
        {

        }

        public static TaskDbContext Create()
        {
            return new TaskDbContext();
        }
    }
}