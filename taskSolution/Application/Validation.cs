﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace taskSolution.Application
{
    public class Validation
    {
        public bool IpValidation(string ip)
        {
            if (String.IsNullOrWhiteSpace(ip))
            {
                return false;
            }

            string[] splitValues = ip.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            return true;
        }
    }
}